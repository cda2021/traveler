compil:
	docker build -t gcccompil ./src
	docker run --name compcont gcccompil
	docker cp compcont:/home/geosearch ./bin
	docker cp compcont:/home/distancecalculator ./bin
	docker stop compcont
	docker rm compcont

compil-no-cache:
	docker build --no-cache -t gcccompil ./src
	docker run --name compcont gcccompil
	docker cp compcont:/home/traveler ./bin
	docker stop compcont
	docker rm compcont

build-docker:
	docker-compose -f docker/docker-compose.yml up