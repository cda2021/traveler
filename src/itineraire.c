#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

# define M_PI           3.14159265358979323846  /* pi */

double radians(double angle)
{
    /* conversion en radians d'un angle en degrés décimaux */
    return angle * M_PI / 180;
}

double distancegps(double lon1, double lat1, double lon2, double lat2)
{
    double distance;
    /*
        Les paramètres sont donnés en longitude et latitude décimale exprimés en degrés.
        Le formulaire de référence à utiliser peut être celui-ci :
        dλ = λB – λA
        SA-B = arc cos (sin φA sin φB + cos φA cos φB cos dλ)
        où les longitudes sont identifiées par φ et les latitudes par λ.
        La valeur obtenue est donnée en référence au rayon moyen de la Terre : 6378.137
        exprimé en kilomètres.
        La valeur de retour est la distance dans l'unité utilisée pour le rayon.
    */
    distance = acos(
                    sin(radians(lon1)) *
                    sin(radians(lon2))
                    +
                    cos(radians(lon1)) *
                    cos(radians(lon2)) *
                    cos(radians(lat2-lat1))
                    ) * 6378.137;

    return distance;
}

int main(int argc, char *argv[])
{
    if (argc < 6)
    {
        printf("Error there must be at least 6 args\n");
        exit(0);
    }
    int i;
    const char *nom;
    const char *insee;
    double lon, lat;
    double longueur_etape, longueur_itineraire;
    int nbrVille = (argc-1)/3;
    int nbrArg = 1;

    typedef struct
    {
        char nom[80];
        double lon;
        double lat;
    } LOCALITE;

    LOCALITE Itineraire[100];

    for(int i=0; i < nbrVille; i++)
    {
        strcpy(Itineraire[i].nom, argv[nbrArg]); nbrArg++;
        sscanf(argv[nbrArg], "%lf", &Itineraire[i].lon); nbrArg++;
        sscanf(argv[nbrArg], "%lf", &Itineraire[i].lat); nbrArg++;
    }

    printf("{\"itineraires\":[");
    if ((argc - 1) > 1)
    {
        longueur_itineraire = 0;
        for(int i=0; i < nbrVille -1 ; i++)
        {
            longueur_etape = distancegps(Itineraire[i].lon, Itineraire[i].lat, Itineraire[i + 1].lon, Itineraire[i + 1].lat);
            longueur_itineraire = longueur_itineraire + longueur_etape;
            printf("{\"depart\": \"%s\", \"arrivee\": \"%s\", \"distance\": %lf}", Itineraire[i].nom, Itineraire[i+1].nom, longueur_etape);

            if (Itineraire[i+2].lat != 0)
            {
                printf(",");
            }
        }
    }

    printf("], \"distance_totale\": \"%lf\"}", longueur_itineraire);
    return 0;
}
