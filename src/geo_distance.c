#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

# define M_PI 3.14159265358979323846  /* pi */

double radians(double angle)
{
    /* conversion en radians d'un angle en degrés décimaux */
    return angle * M_PI / 180;
}

double distancegps(double lon1, double lat1, double lon2, double lat2)
{
    double distance;
    /*
        Les paramètres sont donnés en longitude et latitude décimale exprimés en degrés.
        Le formulaire de référence à utiliser peut être celui-ci :
        dλ = λB – λA
        SA-B = arc cos (sin φA sin φB + cos φA cos φB cos dλ)
        où les longitudes sont identifiées par φ et les latitudes par λ.
        La valeur obtenue est donnée en référence au rayon moyen de la Terre : 6378.137
        exprimé en kilomètres.
        La valeur de retour est la distance dans l'unité utilisée pour le rayon.
    */
    distance = acos(sin(radians(lon1)) * sin(radians(lon2))+cos(radians(lon1)) *cos(radians(lon2)) *cos(radians(lat2-lat1))
                            )
                            * 6378.137;
    return distance;
}

int main(int argc, char *argv[])
{
    FILE *fnum;
    int i, size, value, value2;
    const char *filename;
    const char *mode;
    char record[1000];
    char *pfield;
    char *field;
    const char *nom;
    const char *cp;
    const char *insee;
    double lon, lat;
    double longueur_etape, longueur_itineraire;
    typedef struct
    {
        char nom[80];
        char cp[5];
        char insee[5];
        double lon;
        double lat;
    } LOCALITE;

    LOCALITE Itineraire[100];

    char List_cp[30][30];
    for(int i=0; i < argc-1; i++)
    {
        strcpy(List_cp[i], argv[i+1]);
    }

    filename = "villes_france.txt";
    mode = "r";
    fnum = fopen(filename, mode);
    if (!fnum)
    {
        printf("pb d'accès au fichier\n");
    }
    else
    {
        printf("{\"Villes\":[");
        int j = 0;
        fgets(record, 1000, fnum);
        while(fgets(record, 1000, fnum) != NULL)
        {
            int i = 0;
            int toknum = 0;
            char *src = strdup(record);
            const char delimiters[] = "\n\t";
            const char *token = strtok(record, delimiters);
            while (token != NULL)
            {
                switch (toknum)
                {
                    case 0: nom = token; break;
                    case 1: cp = token; break;
                    case 2: insee = token; break;
                    case 3: sscanf(token, "%lf", &lon); break;
                    case 4: sscanf(token, "%lf", &lat); break;
                    default: printf("sans doute une erreur quelque part dans l'enregistrement %s", record);
                }
                toknum++;
                token = strtok(NULL, delimiters);
            }
            for(i; i < argc-1; i++)
            {
                if ((strcmp(cp, List_cp[i]) == 0) || (strstr(cp, List_cp[i]) != NULL))
                {
                    strcpy(Itineraire[j].nom, nom);
                    strcpy(Itineraire[j].cp, cp);
                    strcpy(Itineraire[j].insee, insee);
                    Itineraire[j].lon = lon;
                    Itineraire[j].lat = lat;
                    j++;
                }
            }

        }
    }
    fclose(fnum);
    for (int i = 0; i < (sizeof(Itineraire)/sizeof(Itineraire[0])); i++)
    {
        if (Itineraire[i].lat == 0)
        {
            break;
        }
        printf("{\"ville\": \"%s\",\"cp\": \"%.5s\",\"insee\": \"%s\",\"lon\": \"%lf\",\"lat\": \"%lf\"}", Itineraire[i].nom, Itineraire[i].cp, Itineraire[i].insee, Itineraire[i].lon, Itineraire[i].lat);
        if (Itineraire[i+1].lat != 0)
        {
            printf(",");
        }
    }

    printf("]}");
    return 0;
}
