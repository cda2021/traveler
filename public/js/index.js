$("#ville").hide();
$("#villeDepart").hide();
$("#villeArrive").hide();

document.querySelector("#traveler").addEventListener('submit', (event) => {
    event.preventDefault();
    let cpDepart = document.querySelector("#depart").value;
    let cpArrivee = document.querySelector("#arrivee").value;

    if (formValidate(cpDepart, cpArrivee)) {
        clearErrors();

        let formData = new FormData();

        formData.append("depart", cpDepart);
        formData.append("arrivee", cpArrivee);
        formData.append("status", "recherche");

        fetch('api.php', {method: 'POST', body: formData}).then((response) =>
        {
            return response.json();
        }).then((data) =>
        {
            $("#ville").show();
            $("#villeDepart").empty();
            $("#villeArrive").empty();

            var villeDepart = [];
            var villeArrive = [];

        
            $.each(data.Villes, function(key, uneVille)
            {
                switch(uneVille.cp)
                {
                    case $("#depart").val(): villeDepart.push(uneVille); break;
                    case $("#arrivee").val(): villeArrive.push(uneVille); break;
                }

            })

            afficheVille(villeDepart, "Depart");
            afficheVille(villeArrive, "Arrive")

            function afficheVille(mesVilles, position)
            {
                if(mesVilles.length != 1)
                {
                    $("#ville"+position).show();
                    $("#ville"+position).append(" <option selected>Choisissez une ville</option>");

                    $.each(mesVilles, function(key, uneVille)
                    {
                        var maVille = "<option value='" + uneVille.ville + "' data-insee='" + uneVille.insee + "'>" + uneVille.ville + "</option>";
                    
                        $("#ville"+position).append(maVille);
                    })
                }
                else
                {
                    var maVilleArrive = "<input class='form-control' type='text' class='browser-default custom-select col-md-5 ' data-insee='" + mesVilles[0].insee + "' placeholder='" + mesVilles[0].ville + "' readonly>";
                    $("#div"+position).append(maVilleArrive);
                }
            }
          
        });
    }
});


function formValidate(depart, arrivee) {
    let errors = 0;

    if (!postalCodeValidate(depart)) {
        let container = document.querySelector(".col-depart");
        handleErrorMessage(container, "depart");

        errors++
    }

    if (!postalCodeValidate(arrivee)) {
        let container = document.querySelector(".col-arrivee");
        handleErrorMessage(container, "arrivee");

        errors++
    }

    return errors <= 0;
}

function postalCodeValidate(postalCode) {
    const regex = /\d{5}/gm;
    return regex.test(postalCode);
}

function handleErrorMessage(container, type) {
    let existingError = document.querySelector("#error-" + type);
    let input = document.querySelector("#" + type);

    if (!existingError) {
        let errorElement = document.createElement('div');
        errorElement.classList.add('invalid-feedback');
        errorElement.id = "error-" + type;
        errorElement.textContent = "Le code postal saisi n'est pas valide.";
        container.insertAdjacentElement('beforeend', errorElement);
        input.classList.add('is-invalid');
    }
}

function clearErrors() {
    let errorDepart = document.querySelector("error-depart");
    let errorArrivee = document.querySelector("error-depart");
    let departInput = document.querySelector("#depart");
    let arriveeInput = document.querySelector("#arrivee");

    errorDepart ? document.removeChild(errorDepart) : null;
    errorArrivee ? document.removeChild(errorArrivee) : null;
    departInput.classList.remove("is-invalid");
    arriveeInput.classList.remove("is-invalid");
}
