class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            departText: "",
            arriveeText: "",
            departTextError: false,
            arriveeTextError: false,
            departChoices: [],
            arriveeChoices: [],
            selectedDepartChoice: "",
            selectedArriveeChoice: "",
            searched: false,
            zipCodeRegexp: /\d{5}/gm,
            routes: [],
            distanceTotale: 0,
            newSearch: false,
            newSearchValue: "",
            newSearchChoices: [],
            selectedNewSearchChoice: ""
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDepartChange = this.handleDepartChange.bind(this);
        this.handleArriveeChange = this.handleArriveeChange.bind(this);
        this.handleSelectDepart = this.handleSelectDepart.bind(this);
        this.handleSelectArrivee = this.handleSelectArrivee.bind(this);
        this.handleCalculateSubmit = this.handleCalculateSubmit.bind(this);
        this.resetSearch = this.resetSearch.bind(this);
        this.resetAll = this.resetAll.bind(this);
        this.newEtape = this.newEtape.bind(this);
        this.handleNewSearchChange = this.handleNewSearchChange.bind(this);
        this.handleSelectEtape = this.handleSelectEtape.bind(this);
        this.handlerGetNewEtape = this.handlerGetNewEtape.bind(this);

        }

    handleSubmit(event) {
        event.preventDefault();
        this.resetSearch()
        const formData = new FormData();
        formData.append("depart", this.state.departText);
        formData.append("arrivee", this.state.arriveeText);
        formData.append("status", "recherche");

        fetch('api.php', {method: 'POST', body: formData}).then((response) => {
            return response.json();
        }).then((data) => {
            console.log(data);

            let departChoices = {...this.state}.departChoices;
            let arriveeChoices = {...this.state}.arriveeChoices;

            data.Villes.forEach((ville) => {
                switch (ville.cp) {
                    case this.state.departText:
                        departChoices.push(ville);
                        break;
                    case this.state.arriveeText:
                        arriveeChoices.push(ville);
                        break;
                }
            });

            this.setState({
                departChoices: departChoices,
                arriveeChoices: arriveeChoices,
                searched: true,
                selectedDepartChoice: departChoices[0].insee,
                selectedArriveeChoice: arriveeChoices[0].insee
            });
        })
    }

    handleDepartChange(event) {
        event.preventDefault();
        if (this.state.zipCodeRegexp.test(event.target.value)) {
            this.setState({departText: event.target.value, departTextError: false})
        } else {
            this.setState({departText: event.target.value, departTextError: true})
        }
    }

    handleArriveeChange(event) {
        event.preventDefault();
        if (this.state.zipCodeRegexp.test(event.target.value)) {
            this.setState({arriveeText: event.target.value, arriveeTextError: false})
        } else {
            this.setState({arriveeText: event.target.value, arriveeTextError: true})
        }
    }

    handleSelectDepart(event) {
        event.preventDefault()
        this.setState({selectedDepartChoice: event.target.value})
    }

    handleSelectArrivee(event) {
        event.preventDefault()
        this.setState({selectedArriveeChoice: event.target.value})
    }

    handleSelectEtape(event)
    {
        event.preventDefault();
        this.setState({selectedNewSearchChoice: event.target.value})
    }

    handleCalculateSubmit(event) {
        event.preventDefault();

        let departCity = this.state.departChoices.find(choice => choice.insee === this.state.selectedDepartChoice);
        let arriveeCity = this.state.arriveeChoices.find(choice => choice.insee === this.state.selectedArriveeChoice);

        const formData = new FormData();
        formData.append("depart_lat_lon", departCity.ville + " " + departCity.lat + " " + departCity.lon);
        formData.append("arrivee_lat_lon", arriveeCity.ville + " " + arriveeCity.lat + " " + arriveeCity.lon);
        formData.append("status", "calcul");

        fetch("api.php", {method: 'POST', body: formData}).then((response) => {
            return response.json();
        }).then((data) => {
            let routes = {...this.state}.routes;
            data.itineraires.forEach(route => routes.push(route));
            console.log(routes)
            let distancetotale = parseInt(this.state.distanceTotale) + parseInt(data.distance_totale);
            console.log(distancetotale)
            this.setState({
                routes: routes,
                distanceTotale: distancetotale
            })
        })
    }

    resetSearch() {
        this.setState({
            departChoices: [],
            arriveeChoices: []
        })
    }

    resetAll()
    {
        this.setState(
            {
                departText: "",
                arriveeText: "",
                departTextError: false,
                arriveeTextError: false,
                departChoices: [],
                arriveeChoices: [],
                selectedDepartChoice: "",
                selectedArriveeChoice: "",
                searched: false,
                zipCodeRegexp: /\d{5}/gm,
                routes: [],
                distanceTotale: 0,
                newSearch: false,
                newSearchValue: "",
                newSearchChoices: [],
                selectedNewSearchChoice: ""
            })
    }

    newEtape()
    {
        let actualState = this.state.newSearch;
        this.setState({newSearch: !actualState});
    }

    handleNewSearchChange(event)
    {
        event.preventDefault()
        this.setState({newSearchValue: event.target.value})
    }

    handlerGetNewEtape(event)
    {
        event.preventDefault();
      
        const formData = new FormData();
        console.log(this.state.newSearchValue);
        formData.append("etape", this.state.newSearchValue);
        formData.append("status", "recherche");

        fetch('api.php', {method: 'POST', body: formData}).then((response) => {
            return response.json();
        }).then((data) => {
            console.log(data);

  
            let etapeChoices = {...this.state}.newSearchChoices;

            data.Villes.forEach((ville) => {
                if(ville.cp == this.state.newSearchValue)
                {
                    etapeChoices.push(ville);
                }
                
            });

            this.setState({
                
                newSearchChoices: etapeChoices,
                searched: true,
                selectedNewSearchChoice: etapeChoices[0].insee
            });
        })
    }


    render() {
        return (<div>
            <form id="traveler" className="form-group needs-validation" noValidate onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col col-depart">
                        <label htmlFor="depart">Départ</label>
                        <input readOnly={this.state.searched} value={this.state.departText} onChange={this.handleDepartChange} minLength={5} maxLength={5} type="text" className={this.state.departTextError ? "form-control is-invalid": "form-control"} placeholder="Ex: 45000"/>
                    </div>
                    <div className="col col-arrivee">
                        <label htmlFor="arrivee">Arrivée</label>
                        <input readOnly={this.state.searched} value={this.state.arriveeText} onChange={this.handleArriveeChange} minLength={5} maxLength={5} type="text" className={this.state.arriveeTextError ? "form-control is-invalid": "form-control"} placeholder="Ex: 75001"/>
                    </div>
                </div>
                <div className="row p-3">
                    <button disabled={this.state.searched || this.state.arriveeTextError || this.state.departTextError} type={"submit"} className="btn btn-block btn-primary">Rechercher</button>
                </div>
            </form>

            <div className="row justify-content-center">
                <button className="btn btn-primary col-md-6" onClick={this.newEtape}>Ajouter une étape !    </button>
            </div>
            
                {this.state.searched ?
                <React.Fragment>
                    {this.state.newSearch ?
                        <div>
                            <input type="text" className="form-control" value={this.state.newSearchValue} onChange={this.handleNewSearchChange} minLength={5} maxLength={5}/>
                            <button className="btn btn-block btn-primary" onClick={this.handlerGetNewEtape}>Rechercher</button>
                            {this.state.newSearchChoices.length > 0 ?
                                <div className="col">
                                    <select onChange={this.handleSelectEtape} disabled={this.state.newSearchChoices.length === 1} value={this.state.selectedNewSearchChoice} className="browser-default custom-select col-md-12 ">
                                        {
                                            this.state.newSearchChoices.map(choice => <option key={choice.insee} value={choice.insee} defaultValue={choice.insee} >{choice.ville}</option>)
                                        }
                                    </select>
                                </div> :null
                            }
                            
                        </div>
                        
                        : null

                    }
                    <form className="form-group" onSubmit={this.handleCalculateSubmit}>
                        <div className="row">
                            <div className="col">
                                <select onChange={this.handleSelectDepart} disabled={this.state.departChoices.length === 1} value={this.state.selectedDepartChoice} className="browser-default custom-select col-md-12 ">
                                    {
                                        this.state.departChoices.map(choice => <option key={choice.insee} value={choice.insee} defaultValue={choice.insee} >{choice.ville}</option>)
                                    }
                                </select>
                            </div>
                            <div className="col">
                                <select onChange={this.handleSelectArrivee} disabled={this.state.arriveeChoices.length === 1} value={this.state.selectedArriveeChoice} className="browser-default custom-select col-md-12 ">
                                    {
                                        this.state.arriveeChoices.map(choice => <option key={choice.insee} value={choice.insee} defaultValue={choice.insee} >{choice.ville}</option>)
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="row p-3">
                            <button className="btn btn-block btn-success">Calculer l'itinéraire</button>
                        </div>
                    </form>

                    <h5 className={"text-center"}>{parseInt(this.state.distanceTotale)} km au total</h5>
                    {this.state.routes.map((route, index) => <Route key={index} distanceTotale={this.state.distanceTotale} route={route}/>)}
                    

                </React.Fragment> : null
                }

        </div>
        )
    }
}

function Route(props) {
    return <React.Fragment>
        <div className="row">
            <div className="col-md-3">
                <div className="card">
                    <div className="card-header">{props.route.depart}</div>
                </div>
            </div>
            <div className="col-md-6">
                <ol></ol>
                <h5 className={"text-center"}>{props.route.distance}</h5>
            </div>
            <div className="col-md-3">
                <div className="card">
                    <div className="card-header">{props.route.arrivee}</div>
                </div>
            </div>
        </div>
    </React.Fragment>
}


ReactDOM.render(<Form/>, document.querySelector("#app"));
