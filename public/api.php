<?php
// Affiche le nom d'utilisateur qui fait tourner le processus php/http
// (sur un système ayant "whoami" dans le chemin d'exécutables)
$cpDepart=null;
$cpArrivee=null;

if(isset($_REQUEST["depart"]) and isset($_REQUEST["arrivee"])) {

    if ((preg_match("/\d{5}/", $_REQUEST["depart"])==0) or (preg_match("/\d{5}/", $_REQUEST["arrivee"])== 0))
    {
        http_response_code(404);
        die;
    }

    // recherche et calcul
    if(isset($_REQUEST["status"]) and ($_REQUEST["status"] == "recherche"))
    {
        $output=null;
        $retval=null;
        exec("/home/bin/geosearch {$_REQUEST["depart"]} {$_REQUEST["arrivee"]}", $output, $retval);
        $encoded = ($output[0]);
        echo $encoded;
    }

    else{
        //http_response_code(404);
    }
}
if(isset($_REQUEST["depart_lat_lon"]) and isset($_REQUEST["arrivee_lat_lon"])and isset($_REQUEST["status"]) and ($_REQUEST["status"] == "calcul"))
{
    if ((preg_match("/\w+\s\d+\.\d+\s\d+\.\d+/", $_REQUEST["depart_lat_lon"])==0) or (preg_match("/\w+\s\d+\.\d+\s\d+\.\d+/", $_REQUEST["arrivee_lat_lon"])== 0))
    {
        http_response_code(404);
        die;
    }

    //add calcul here
    $output=null;
    $retval=null;
    exec("/home/bin/distancecalculator {$_REQUEST["depart_lat_lon"]} {$_REQUEST["arrivee_lat_lon"]}", $output, $retval);
    $encoded = ($output[0]);
    echo $encoded;
}

if(isset($_REQUEST["etape"]) ) {

    if ((preg_match("/\d{5}/", $_REQUEST["etape"])==0))
    {
        http_response_code(404);
        die;
    }

    // recherche et calcul
    if(isset($_REQUEST["status"]) and ($_REQUEST["status"] == "recherche"))
    {
        $output=null;
        $retval=null;
        exec("/home/bin/geosearch {$_REQUEST["etape"]} ", $output, $retval);
        $encoded = ($output[0]);
        echo $encoded;
    }
}

?>
